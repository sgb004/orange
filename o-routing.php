<?php
$routes = array(
	'builder_home' => array(
		'paths' => array('/admin/builder/'),
		'm' => 'builder_page',
		'c' => 'BuilderPage',
		'v' => 'index'
	),
	'install_save' => array(
		'paths' => array('/install/save'),
		'm' => 'orange',
		'c' => 'Install',
		'v' => 'save'
	),
	'install' => array(
		'paths' => array('/'),
		'm' => 'orange',
		'c' => 'Install',
		'v' => 'install'
	)
)
?>