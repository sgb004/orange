document.addEventListener('DOMContentLoaded', builderPagesReady);

function builderPagesReady(){
	var fbOptions = {
		onSave: function(e, formData) {
			var renderedForm = $('<div>');
			renderedForm.formRender({
				formData: formData
			});
			console.log( renderedForm.html() );
			/*
			toggleEdit();
			$('.render-wrap').formRender({
				formData: formData,
				templates: templates
			});
			window.sessionStorage.setItem('formData', JSON.stringify(formData));
			*/
		}
	}
	var formBuilder = $('#fb-editor').formBuilder( fbOptions );
	var fbPromise = formBuilder.promise;

	fbPromise.then(function(fb) {
		fb.actions.setLang('es-ES');
	});
}