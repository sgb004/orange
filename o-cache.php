<?php
/**
 * @author @sgb004
 * @version 0.1.0
 */

$path = __DIR__;
if($path === '__DIR__'){
	$path = getcwd();
}

define('ABSPATH', $path.'/');

$filesList = glob(ABSPATH.'o_cache/*');

foreach ($filesList as $file) {
	if( is_dir($file) ){
		$moreFilesList = glob($file.'/*');
		foreach ($moreFilesList as $moreFile) {
			if(is_file($moreFile)){
				if( !unlink($moreFile) ){
					echo '<pre>';
					print_r( 'Ocurrió un error al borrar el archivo '.$moreFile );
					echo '</pre>';
				}
			}
		}
		if( !rmdir($file) ){
			echo '<pre>';
			print_r( 'Ocurrió un error al borrar el directorio '.$file );
			echo '</pre>';
		}
	}else if(is_file($file)){
		unlink($file);
	}
}
?>
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
	<title>Borrar caché</title>
</head>
<body>
	
</body>
</html>