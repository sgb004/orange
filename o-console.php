<?php
$path = __DIR__;
if($path === '__DIR__'){
	$path = getcwd();
}

$_SERVER = array(
	'DOCUMENT_ROOT' => $path,
	'REQUEST_URI' => '/',
	'HTTP_HOST' => 'localhost',
	'REQUEST_METHOD' => ''
);

require_once 'orange.php';
?>