<?php
/**
 * Permite imprimir un template del modulo, se sustituye todo el codigo para dar paso a Twig
 * @class Template
 * @version 3.1.3
 * @author @sgb004
 */

class TemplateBase{
	static public $twig;
	static public $url = '';
	static public $urlTemplate = '';
	static public $path = 'o_templates';
	static public $pathTemplate = '';

	/**
	 * Inicia Twig
	 */
	static function init(){
		if( !self::$twig ){
			self::$url = URL.self::$path.'/';
			self::$urlTemplate = self::$url.$GLOBALS['template'].'/';
			self::$path = ABSPATH.self::$path.'/';
			self::$pathTemplate = self::$path.$GLOBALS['template'].'/';

			require_once ABSPATH.'/vendor/autoload.php';
			$twigLoader = new \Twig\Loader\FilesystemLoader(self::$pathTemplate);
			self::$twig = new \Twig\Environment($twigLoader, Template::getSettings());

			/*
			Twig_Autoloader::register();
			$twigLoader = new Twig_Loader_Filesystem(self::$pathTemplate);
			self::$twig = new Twig_Environment($twigLoader, Template::getSettings());
			*/
			Template::customFunctions();
			self::$twig->addGlobal( 'url', URL );
			self::$twig->addGlobal( 'abs_url', ABSURL );
			self::$twig->addGlobal( 'full_url', URL.ROUTE );
			self::$twig->addGlobal( 'notices', new Notices() );
			self::$twig->addGlobal( 'session', false );
		}
	}

	/**
	 * Carga las opciones con las que funcionará twig
	 */
	protected static function getSettings(){
		return array();
	}

	/**
	 * Carga las funciones personalizadas
	 */
	protected static function customFunctions(){
		$notices_print = new Twig_SimpleFunction('notices_print', function( $template ){
			$notices = Notices::get();
			Notices::clean();
			return Template::render( $template, array('notices' => $notices) ); 
		});
		Template::$twig->addFunction( $notices_print );
	}

	/**
	 * Rederiza la vista
	 */
	static function render( $view, $options = array() ){
		echo self::$twig->render($view, $options);
	}

	/**
	 * Rederiza la vista, pero la devuelve en forma de string
	 */
	static function getView( $view, $options = array() ){
		return self::$twig->render($view, $options);
	}

	/**
	 * Imprime un array en un JSON
	 */
	static function renderJson( $data ){
		echo json_encode($data);
		exit;
	}

	/**
	 * Imprime un array en un JSON
	 */
	static function renderRoute( $route ){
		$route = substr($route, 1);

		if( $route == '' ){
			$route = 'index.html';
		}

		$file = $fileAlt = self::$pathTemplate.$route;
		if( is_dir($file) ){
			$route .= 'index.html';
		}

		$isHtml = substr($route, -5);

		if( $isHtml !== '.html' ){
			$route .= '.html';
			$fileAlt .= '.html.twig'; 
		}

		if( file_exists($file) ){
			self::render($route);
		}else if( $file == $fileAlt ){
			self::renderError404();
		}else if( file_exists($fileAlt) ){
			$route .= '.twig'; 
			self::render($route);
		}else{
			self::renderError404();
		}
	}

	/**
	 * Imprime una vista mostrando el tipico error 404
	 */
	static function renderError404(){
		$r = ROUTE;
		$route = explode('/', ROUTE);
		foreach ($route as $k => $path) {
			$file = implode('/', $route);
			$file .= '/404.html.twig';
			$path = self::$pathTemplate.$file;
			$file = str_replace('//', '/', $file);
			$path = str_replace('//', '/', $path);
			if( file_exists($path) ){ break; }
			array_pop($route);
		}

		header("HTTP/1.0 404 Not Found");
		self::render( $file );
	}
}
?>