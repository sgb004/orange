<?php
/**
 * Carga la lista de mensajes
 * @class Notices
 * @version 1.0.1
 */

class Notices{
	static $notices = array();

	static public function init(){
		if( isset( $_SESSION[ SESSION_NAME.'_notices' ] ) ){
			self::$notices = $_SESSION[ SESSION_NAME.'_notices' ];
		}
	}

	static public function add( $type, $notice ){
		self::init();

		if( !isset(self::$notices[$type]) ){
			self::$notices[$type] = array();
		}

		self::$notices[$type][] = $notice;
		$_SESSION[ SESSION_NAME.'_notices' ] = self::$notices;
	}

	static public function get(){
		self::init();
		return self::$notices;
	}

	static public function clean(){
		$_SESSION[ SESSION_NAME.'_notices' ] = array();
	}
}
?>