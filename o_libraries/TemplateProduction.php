<?php
/**
 * Clase que se usará en production
 * @class Template
 * @version 1.0.1
 * @author @sgb004
 */

class Template extends TemplateBase{
	static function init(){
		parent::init();
	}

	protected static function getSettings(){
		return array(
			'cache' => ABSPATH.'/o_cache/'
		);
	}

	protected static function customFunctions(){
		parent::customFunctions();
		$twigFunction = new Twig_SimpleFunction('is_page_speed', function(){
			return ( stripos($_SERVER['HTTP_USER_AGENT'],'Insights') === false ) ? false : true; 
		});
		self::$twig->addFunction( $twigFunction );
	}
}
?>