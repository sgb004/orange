<?php
//DATABASE
define('DB_HOST', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_DATABASE', '');

define('DB_CHARSET', 'utf8');

//DATE
date_default_timezone_set('America/Mexico_City');

//MAIL
define('ADMIN_MAIL', 'example@site.com');
define('NO_REPLY_MAIL', 'example@site.com');
define('NO_REPLY_MAIL_NAME', 'Orange');

//SITE URL
define('SITE_URL', 'http://site.com/');

//MODULES
$MODULES['orange'] = array('controller' => 'prueba');
$MODULES['form'] = array();
$MODULES['builder_page'] = array();

//DIRS
define('O_SRCS','o_srcs/');
define('O_LIBRARIES','o_libraries/');

//SESSION NAME
define('SESSION_NAME', 'orange');

//DEBUG
define('IS_DEBUG', true);

//TEMPLATE
$template = 'install';
//$template = 'default';
?>