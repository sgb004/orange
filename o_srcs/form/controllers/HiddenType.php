<?php
/**
 * @author @sgb004
 * @version 0.1.0
 */

class HiddenType extends TextType{
	const NAME = 'HiddenType';
	protected $type = 'hidden';
}
?>