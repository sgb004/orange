<?php
/**
 * @author @sgb004
 * @version 0.1.0
 */

class TypeUtil{
	public static function renderAttrs( $attrs ){
		$a = '';
		foreach ($attrs as $key => $value) {
			if( is_string( $value ) ){
				$value = trim( $value );
				$a .= $key.'="'.$value.'" ';
			}
		}
		$e = trim( $a );
		return $a;
	}
}
?>